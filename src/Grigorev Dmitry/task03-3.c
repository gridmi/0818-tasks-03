#include <stdio.h>
#include <stdlib.h>
#define N 10
int **mas,len[100],id[100];
int main() {
	char c;
	int s = 0;
	for (int i = 0; i < N; i++)
		id[i] = i;
	mas = (int**)malloc(N*sizeof(int*));
	for (int i = 0; i < N; i++) {
		int fl = 0;
		char str[100];
		while ((c = getchar()) != '\n') {
			str[fl] = c;
			fl++;
		}
		if (fl == 0)
			break;
		s++;
		mas[i] = (int*)malloc(fl*sizeof(int));
		for (int j = 0; j < fl; j++)
			mas[i][j] = str[j];
		len[i] = fl;
	}
	for (int i = 0; i < s; i++)
		for (int j = 0; j < i;j++)
			if (len[j]>len[i]) {
				int x = len[i];
				len[i] = len[j];
				len[j] = x;
				int y = id[i];
				id[i] = id[j];
				id[j] = y;
			}
	for (int i = 0; i < s; i++) {
		for (int j = 0; j < len[i]; j++)
			printf("%c", mas[id[i]][j]);
		printf("\n");
	}
	for (int i = 0; i < s; i++)
		free(mas[i]);
	free(mas);
	return 0;
}
