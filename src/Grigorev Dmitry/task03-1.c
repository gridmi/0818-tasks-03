#include <stdio.h>
#include <stdlib.h>
#define N 10
int **mas,len[100];
int main() {
	char c;
	int s = 0;
	mas = (int**)malloc(N*sizeof(int*));
	for (int i = 0; i < N; i++) {
		int fl = 0;
		char str[100];
		while ((c = getchar()) != '\n') {
			str[fl] = c;
			fl++;
		}
		if (fl == 0)
			break;
		s++;
		mas[i] = (int*)malloc(fl*sizeof(int));
		for (int j = 0; j < fl; j++)
			mas[i][j] = str[j];
		len[i] = fl;
	}
	for (int i = s-1; i >= 0; i--)
		for (int j = 0; j < len[i]; j++)
			printf("%c", mas[i][j]);
	for (int i = 0; i < s; i++)
		free(mas[i]);
	free(mas);
	return 0;
}
