#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10
int **mas,len[100],id[100];
int main() {
	char c;
	int s = 0;
	for (int i = 0; i < N; i++)
		id[i] = 1;
	mas = (int**)malloc(N*sizeof(int*));
	for (int i = 0; i < N ; i++) {
		int fl = 0;
		char str[100];
		while ((c = getchar()) != '\n') {
			str[fl] = c;
			fl++;
		}
		if (fl == 0)
			break;
		s++;
		mas[i] = (int*)malloc(fl*sizeof(int));
		for (int j = 0; j < fl; j++)
			mas[i][j] = str[j];
		len[i] = fl;
	}
	srand(time(0));
	int x = s, y = 0;
	for (int i = 0; x > 0; i++) {
		y = rand() % s;
		if (id[y] == 0)
			continue;
		for (int j = 0; j < len[y]; j++)
			printf("%c", mas[y][j]);
		printf("\n");
		id[y] = 0;
		x--;
	}
	for (int i = 0; i < s; i++)
		free(mas[i]);
	free(mas);
	return 0;
}
